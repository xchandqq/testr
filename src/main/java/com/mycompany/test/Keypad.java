/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.test;

import com.mycompany.test.inter.KeypadInterface;
import java.util.Scanner;

/**
 *
 * @author Christian
 */
public class Keypad implements KeypadInterface{
        
    @Override
    public String captureAmount() {
        try{
            Scanner scanner = new Scanner(System.in);
            return scanner.nextLine();
        }
        catch(Exception e){
            return null;
        }
    }
    
}
