/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.test;

import com.mycompany.test.inter.ScreenInterface;

/**
 *
 * @author Christian
 */
public class Screen implements ScreenInterface{

    @Override
    public void askForAmount() {
        System.out.println("Enter amount: ");
    }

    @Override
    public void displayInvalidAmount() {
        System.out.println("You have entered an invalid amount. Please try again.");
    }

    @Override
    public void displayInvalidDenomination() {
        System.out.println("We're sorry. This machine can only dispense 50, 100, 500, and 1000 bills");
        System.out.println("Please try again.");
    }

    @Override
    public void displayInsufficientAmount() {
        System.out.println("We're sorry. There is not enough amount left in this machine.");
        System.out.println("Please call the administrator.");
    }

    @Override
    public void displaySuccess() {
        System.out.println("Please take the money from the dispenser. Thank you.");
    }

    @Override
    public void displayProcessing() {
        System.out.println("Processing...");
    }

    @Override
    public void displayInsufficientDenominations() {
        System.out.println("We're sorry. The machine cannot dispense enough bills.");
        System.out.println("Please call the administrator.");
    }
    
}
