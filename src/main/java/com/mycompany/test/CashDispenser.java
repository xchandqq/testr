/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.test;

import com.mycompany.test.inter.DispenserInterface;
import java.util.Arrays;

/**
 *
 * @author Christian
 */
public class CashDispenser implements DispenserInterface{
    
    public final static int BILL_ONE_THOUSAND   = 0;
    public final static int BILL_FIVE_HUNDRED   = 1;
    public final static int BILL_ONE_HUNDRED    = 2;
    public final static int BILL_FIFTY          = 3;
    
    public final static int ERROR_TYPE_INVALID       = 0;
    public final static int ERROR_TYPE_DENOMINATION  = 1;
    public final static int ERROR_TYPE_INSUFFICIENT  = 2;
    public final static int ERROR_TYPE_NONE          = 3;
    
    private final int[] billCount = new int[4];

    public CashDispenser(int[] billCount) throws IllegalAccessException {
        if(this.billCount.length != billCount.length) throw new IllegalAccessException("length mismatch");
        System.arraycopy(billCount, 0, this.billCount, 0, 4);
    }    

    @Override
    public boolean dispense(String s) {
        int amount = Integer.parseInt(s);
        
        int oneThousandBills = Math.min(amount/1000, billCount[BILL_ONE_THOUSAND]);
        amount -= (oneThousandBills*1000);
        
        int fiveHundredBills =  Math.min(amount/500, billCount[BILL_FIVE_HUNDRED]);
        amount -= (fiveHundredBills*500);
        
        int oneHundredBills =  Math.min(amount/100, billCount[BILL_ONE_HUNDRED]);
        amount -= (oneHundredBills*100);
        
        int fiftyBills =  Math.min(amount/50, billCount[BILL_FIFTY]);
        amount -= (fiftyBills*50);
        
        if(amount > 0) return false;
        
        billCount[BILL_ONE_THOUSAND] -= oneThousandBills;
        billCount[BILL_FIVE_HUNDRED] -= fiveHundredBills;
        billCount[BILL_ONE_HUNDRED] -= oneHundredBills;
        billCount[BILL_FIFTY] -= fiftyBills;
        return true;
    }

    @Override
    public int isAmountValid(String s) {  
        if(s == null) return ERROR_TYPE_INVALID;
        
        int amount;
        try{
            amount = Integer.parseInt(s);
        }
        catch(NumberFormatException e){
            return ERROR_TYPE_INVALID;
        }
        
        if(amount == 0) return ERROR_TYPE_INVALID;
        if(amount%50 != 0) return ERROR_TYPE_DENOMINATION;
        if(amount > getRemainingAmount()) return ERROR_TYPE_INSUFFICIENT;
        return ERROR_TYPE_NONE;
    }

    @Override
    public int getRemainingAmount() {
        return
                billCount[BILL_ONE_THOUSAND] * 1000 +
                billCount[BILL_FIVE_HUNDRED] * 500 +
                billCount[BILL_ONE_HUNDRED] * 100 +
                billCount[BILL_FIFTY] * 50;
    }
    
}
