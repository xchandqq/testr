/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.test;

/**
 *
 * @author Christian
 */
public class ATM {
    
    private final Screen screen;
    private final Keypad keypad;
    private final CashDispenser cashDispenser;

    public ATM(Screen screen, Keypad keypad, CashDispenser cashDispenser) {
        this.screen = screen;
        this.keypad = keypad;
        this.cashDispenser = cashDispenser;
    }
    
    public void withdraw(){
        screen.askForAmount();
        
        String enteredAmount = keypad.captureAmount();
        screen.displayProcessing();
        
        switch(cashDispenser.isAmountValid(enteredAmount)){
            case CashDispenser.ERROR_TYPE_INVALID:
                screen.displayInvalidAmount();
                break;
            case CashDispenser.ERROR_TYPE_DENOMINATION:
                screen.displayInvalidDenomination();
                break;
            case CashDispenser.ERROR_TYPE_INSUFFICIENT:
                screen.displayInsufficientAmount();
                return;
            case CashDispenser.ERROR_TYPE_NONE:
                if(cashDispenser.dispense(enteredAmount)) screen.displaySuccess();
                else screen.displayInsufficientDenominations();
                break;
        }
    }
    
}
