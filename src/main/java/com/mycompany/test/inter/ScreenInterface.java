/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.test.inter;

/**
 *
 * @author Christian
 */
public interface ScreenInterface {
    void askForAmount();
    void displayInvalidAmount();
    void displayInvalidDenomination();
    void displayInsufficientAmount();
    void displayInsufficientDenominations();
    void displaySuccess();
    void displayProcessing();
}
