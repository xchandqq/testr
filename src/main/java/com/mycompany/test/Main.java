/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.test;

/**
 *
 * @author Christian
 */
public class Main {
    
    public static void main(String[] args) throws IllegalAccessException {
        Screen screen = new Screen();
        Keypad keypad = new Keypad();
        CashDispenser dispenser = new CashDispenser(new int[]{5,5,5,5});
        ATM atm = new ATM(screen, keypad, dispenser);
        
        while(true){
            atm.withdraw();
        }
    }
}
